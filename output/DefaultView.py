import sys

import pyqtgraph as pg
from PyQt6 import QtCore
from PyQt6.QtGui import QFont
from PyQt6.QtWidgets import QApplication, QLabel, QWidget, QHBoxLayout, QVBoxLayout


class DefaultView:
    __data = None
    __local = None

    def update(self):

        # PyQt Update
        self.__data.parse()

        self.value_temperature.setText(str(round(self.__data.get_temperature(), 1)))
        self.value_humidity.setText(str(round(self.__data.get_humidity(), 1)))
        self.value_co2.setText(str(round(self.__data.get_co2(), 1)))
        self.value_voc.setText(str(round(self.__data.get_voc(), 1)))
        self.value_light.setText(str(round(self.__data.get_light(), 1)))
        self.value_sound_dBA.setText(str(round(self.__data.get_dba_spl(), 1)))

        self.plot_temperature.clear()
        self.plot_temperature.plot(self.__data.get_temperature_series(), pen = self.pen0)
        self.plot_temperature.setYRange(0, 40)

        self.plot_humidity.clear()
        self.plot_humidity.plot(self.__data.get_humidity_series(), pen = self.pen1)
        self.plot_humidity.setYRange(0, 100)

        self.plot_co2.clear()
        self.plot_co2.plot(self.__data.get_co2_series(), pen = self.pen2)
        self.plot_co2.setYRange(390, 1200)

        self.plot_voc.clear()
        self.plot_voc.plot(self.__data.get_voc_series(), pen = self.pen3)
        self.plot_voc.setYRange(0, 60)

        self.plot_light.clear()
        self.plot_light.plot(self.__data.get_light_series(), pen = self.pen4)
        self.plot_light.setYRange(0, 1000)

        self.plot_dBA.clear()
        self.plot_dBA.plot(self.__data.get_dba_spl_series(), pen = self.pen5)
        self.plot_dBA.setYRange(30, 110)

    def __init__(self, d, l):
        self.__data = d
        self.__local = l

        # Plot's line styles
        self.pen0 = pg.mkPen(color=(255, 0, 0), width = 3)
        self.pen1 = pg.mkPen(color=(255, 40, 0), width=3)
        self.pen2 = pg.mkPen(color=(255, 80, 0), width=3)
        self.pen3 = pg.mkPen(color=(255, 120, 0), width=3)
        self.pen4 = pg.mkPen(color=(255, 160, 0), width=3)
        self.pen5 = pg.mkPen(color=(255, 200, 0), width=3)

        # PyQt Setup
        self.app = QApplication(sys.argv)

        self.win = QWidget()
        self.win.setWindowTitle(self.__local["window_default"])
        self.win.resize(640, 420)

        # text labels
        sansfont = QFont("Helvetica", 38)
        sansfont.setBold(True)

        title_temperature = QLabel()
        title_temperature.setText(self.__local["label_temperature"])

        title_humidity = QLabel()
        title_humidity.setText(self.__local["label_humidity"])

        title_co2 = QLabel()
        title_co2.setText(self.__local["label_co2"])

        title_voc = QLabel()
        title_voc.setText(self.__local["label_voc"])

        title_light = QLabel()
        title_light.setText(self.__local["label_light"])

        title_sound_dBA = QLabel()
        title_sound_dBA.setText(self.__local["label_dba"])

        self.value_temperature = QLabel()
        self.value_temperature.setText("--")
        self.value_temperature.setFont(sansfont)

        self.value_humidity = QLabel()
        self.value_humidity.setText("--")
        self.value_humidity.setFont(sansfont)

        self.value_co2 = QLabel()
        self.value_co2.setText("--")
        self.value_co2.setFont(sansfont)

        self.value_voc = QLabel()
        self.value_voc.setText("--")
        self.value_voc.setFont(sansfont)

        self.value_light = QLabel()
        self.value_light.setText("--")
        self.value_light.setFont(sansfont)

        self.value_sound_dBA = QLabel()
        self.value_sound_dBA.setText("--")
        self.value_sound_dBA.setFont(sansfont)

        self.app_layout = QHBoxLayout()

        self.txt_layout = QVBoxLayout()
        self.txt_layout.addWidget(title_temperature)
        self.txt_layout.addWidget(self.value_temperature)
        self.txt_layout.addStretch()
        self.txt_layout.addWidget(title_humidity)
        self.txt_layout.addWidget(self.value_humidity)
        self.txt_layout.addStretch()
        self.txt_layout.addWidget(title_co2)
        self.txt_layout.addWidget(self.value_co2)
        self.txt_layout.addStretch()
        self.txt_layout.addWidget(title_voc)
        self.txt_layout.addWidget(self.value_voc)
        self.txt_layout.addStretch()
        self.txt_layout.addWidget(title_light)
        self.txt_layout.addWidget(self.value_light)
        self.txt_layout.addStretch()
        self.txt_layout.addWidget(title_sound_dBA)
        self.txt_layout.addWidget(self.value_sound_dBA)
        self.txt_layout.addStretch()
        self.app_layout.addLayout(self.txt_layout)

        self.plot_layout = QVBoxLayout()

        self.plot_temperature = pg.plot()
        self.plot_temperature.hideAxis('bottom')
        self.plot_layout.addWidget(self.plot_temperature)

        self.plot_humidity = pg.plot()
        self.plot_humidity.hideAxis('bottom')
        self.plot_layout.addWidget(self.plot_humidity)

        self.plot_co2 = pg.plot()
        self.plot_co2.hideAxis('bottom')
        self.plot_layout.addWidget(self.plot_co2)

        self.plot_voc = pg.plot()
        self.plot_voc.hideAxis('bottom')
        self.plot_layout.addWidget(self.plot_voc)

        self.plot_light = pg.plot()
        self.plot_light.hideAxis('bottom')
        self.plot_layout.addWidget(self.plot_light)

        self.plot_dBA = pg.plot()
        self.plot_dBA.hideAxis('bottom')
        self.plot_layout.addWidget(self.plot_dBA)

        self.app_layout.addLayout(self.plot_layout)

        timer = QtCore.QTimer()
        timer.timeout.connect(self.update)
        timer.start(125)

        self.win.setLayout(self.app_layout)
        self.win.show()

        sys.exit(self.app.exec())
