import sys

import pyqtgraph as pg
from PyQt6 import QtCore
from PyQt6.QtGui import QFont
from PyQt6.QtWidgets import QApplication, QLabel, QWidget, QHBoxLayout, QVBoxLayout, QSpinBox


class NoiseDBAView:
    __data = None
    __local = None
    __pooling_time = 125  # tiempo de refresco de datos 125|1000ms

    def valuechange(self):
        self.__data.set_noise_calibration(float(self.input_calibration.value()))

    def update(self):


        # actualizo los datos
        self.__data.parse()

        # actualizo los elementos gráficos
        self.value_sound_dBA.setText(str(round(self.__data.get_dba_spl(), 1)))

        self.plot_dba.clear()
        self.plot_dba.plot(self.__data.get_dba_spl_series(), pen = self.pen0)

        self.plot_amplitude.clear()
        self.plot_amplitude.plot(self.__data.get_sound(), pen = self.pen1)

        self.plot_spectrum.clear()
        self.plot_spectrum.plot(self.__data.get_spectrum()[1], pen = self.pen2)

    def __init__(self, d, l):
        self.__data = d
        self.__local = l

        self.pen0 = pg.mkPen(color=(255, 0, 0), width=3)
        self.pen1 = pg.mkPen(color=(255, 125, 0), width=3)
        self.pen2 = pg.mkPen(color=(255, 255, 0), width=3)

        # PyQt Setup
        self.app = QApplication(sys.argv)

        self.win = QWidget()
        self.win.setWindowTitle(self.__local["window_noise"])
        self.win.resize(640, 240)

        # dBA's
        title_sound_dBA = QLabel()
        title_sound_dBA.setText(self.__local["label_dba"])
        self.value_sound_dBA = QLabel()
        sansfont = QFont("Helvetica", 38)
        sansfont.setBold(True)
        self.value_sound_dBA.setFont(sansfont)
        self.value_sound_dBA.setText("--")
        # Calibracion
        title_calibration = QLabel()
        title_calibration.setText(self.__local["label_calibration"])
        self.input_calibration = QSpinBox()
        self.input_calibration.setRange(-20, 20)
        self.input_calibration.setValue(self.__data.get_noise_calibration())
        self.input_calibration.valueChanged.connect(self.valuechange)

        # Layout 1ª columna
        self.txt_layout = QVBoxLayout()
        self.txt_layout.addWidget(title_sound_dBA)
        self.txt_layout.addWidget(self.value_sound_dBA)
        self.txt_layout.addWidget(title_calibration)
        self.txt_layout.addWidget(self.input_calibration)
        self.txt_layout.addStretch()

        # Layout 2ª columna
        self.plot_layout = QVBoxLayout()
        title_dba = QLabel()
        title_dba.setText(self.__local["label_dba"])
        self.plot_dba = pg.plot()
        self.plot_dba.setYRange(30, 120)
        self.plot_dba.hideAxis('bottom')
        self.plot_layout.addWidget(title_dba)
        self.plot_layout.addWidget(self.plot_dba)

        self.plot_amplitude = pg.plot()
        title_amplitude = QLabel()
        title_amplitude.setText(self.__local["label_amplitude"])
        self.plot_amplitude.setYRange(-2000, 2000)
        self.plot_amplitude.hideAxis('bottom')
        self.plot_layout.addWidget(title_amplitude)
        self.plot_layout.addWidget(self.plot_amplitude)

        self.plot_spectrum = pg.plot()
        title_spectrum = QLabel()
        title_spectrum.setText(self.__local["label_spectrum"])
        self.plot_spectrum.setYRange(-500, 40000)
        self.plot_spectrum.hideAxis('bottom')
        self.plot_layout.addWidget(title_spectrum)
        self.plot_layout.addWidget(self.plot_spectrum)

        # Layout aplicación
        self.app_layout = QHBoxLayout()
        self.app_layout.addLayout(self.txt_layout)
        self.app_layout.addLayout(self.plot_layout)

        # Frecuencia de actualización
        timer = QtCore.QTimer()
        timer.timeout.connect(self.update)
        timer.start(self.__pooling_time)

        self.win.setLayout(self.app_layout)
        self.win.show()
        sys.exit(self.app.exec())
