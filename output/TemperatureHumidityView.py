import sys

import pyqtgraph as pg
from PyQt6 import QtCore
from PyQt6.QtGui import QFont
from PyQt6.QtWidgets import QApplication, QLabel, QWidget, QHBoxLayout, QVBoxLayout, QSpinBox


class TemperatureHumidityView:
    __data = None
    __local = None
    __pooling_time = 125  # tiempo de refresco de datos 125|1000ms

    def valuetchange(self):
        self.__data.set_temperature_calibration(float(self.input_calibration_t.value()))

    def valuehchange(self):
        self.__data.set_humidity_calibration(float(self.input_calibration_h.value()))

    def update(self):
        self.__data.parse()
        self.value_temperature.setText(str(round(self.__data.get_temperature(), 1)))
        self.value_humidity.setText(str(round(self.__data.get_humidity(), 1)))

        self.plot_temperature.clear()
        self.plot_temperature.plot(self.__data.get_temperature_series(), pen = self.pen0)

        self.plot_humidity.clear()
        self.plot_humidity.plot(self.__data.get_humidity_series(), pen= self.pen1)

    def __init__(self, d, l):
        self.__data = d
        self.__local = l

        # PyQt Setup
        self.app = QApplication(sys.argv)
        self.win = QWidget()
        self.win.setWindowTitle(self.__local["window_temperature"])
        self.win.resize(640, 240)
        # Plot's line styles
        self.pen0 = pg.mkPen(color=(255, 125, 0), width = 3)
        self.pen1 = pg.mkPen(color=(255, 255, 0), width=3)
        # temperatura
        sansfont = QFont("Helvetica", 38)
        sansfont.setBold(True)
        title_temperature = QLabel()
        title_temperature.setText(self.__local["label_temperature"])
        self.value_temperature = QLabel()
        self.value_temperature.setFont(sansfont)
        self.value_temperature.setText("--")
        # Calibración temperatura
        title_calibration_t = QLabel()
        title_calibration_t.setText(self.__local["label_calibration"])
        self.input_calibration_t = QSpinBox()
        self.input_calibration_t.setRange(-20, 20)
        self.input_calibration_t.setValue(self.__data.get_temperature_calibration())
        self.input_calibration_t.valueChanged.connect(self.valuetchange)
        # humedad
        title_humidity = QLabel()
        title_humidity.setText(self.__local["label_humidity"])
        self.value_humidity = QLabel()
        self.value_humidity.setFont(sansfont)
        self.value_humidity.setText("--")
        # Calibración humedad
        title_calibration_h = QLabel()
        title_calibration_h.setText(self.__local["label_calibration"])
        self.input_calibration_h = QSpinBox()
        self.input_calibration_h.setRange(-20, 20)
        self.input_calibration_h.setValue(self.__data.get_humidity_calibration())
        self.input_calibration_h.valueChanged.connect(self.valuehchange)

        # Layout
        self.txt_layout = QVBoxLayout()
        self.txt_layout.addWidget(title_temperature)
        self.txt_layout.addWidget(self.value_temperature)
        self.txt_layout.addWidget(title_calibration_t)
        self.txt_layout.addWidget(self.input_calibration_t)
        self.txt_layout.addStretch()
        self.txt_layout.addWidget(title_humidity)
        self.txt_layout.addWidget(self.value_humidity)
        self.txt_layout.addWidget(title_calibration_h)
        self.txt_layout.addWidget(self.input_calibration_h)
        self.txt_layout.addStretch()

        self.plot_layout = QVBoxLayout()
        self.plot_temperature = pg.plot()
        self.plot_temperature.setYRange(-10, 40)
        self.plot_temperature.hideAxis('bottom')
        self.plot_layout.addWidget(self.plot_temperature)
        self.plot_humidity = pg.plot()
        self.plot_humidity.setYRange(0, 100)
        self.plot_humidity.hideAxis('bottom')
        self.plot_layout.addWidget(self.plot_humidity)

        self.app_layout = QHBoxLayout()
        self.app_layout.addLayout(self.txt_layout)
        self.app_layout.addLayout(self.plot_layout)

        # Update Timer
        timer = QtCore.QTimer()
        timer.timeout.connect(self.update)
        timer.start(self.__pooling_time)

        self.win.setLayout(self.app_layout)
        self.win.show()
        sys.exit(self.app.exec())
