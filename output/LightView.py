import sys

import pyqtgraph as pg
from PyQt6 import QtCore
from PyQt6.QtGui import QFont
from PyQt6.QtWidgets import QApplication, QLabel, QWidget, QHBoxLayout, QVBoxLayout, QSpinBox


class LightView:
    __data = None
    __local = None
    __pooling_time = 125  # tiempo de refresco de datos 125|1000ms

    def valuechange(self):
        self.__data.set_light_calibration(float(self.input_calibration.value()))

    def update(self):
        self.__data.parse()
        self.value_light.setText(str(round(self.__data.get_light(), 1)))

        self.plot_light.clear()
        self.plot_light.plot(self.__data.get_light_series(), pen = self.pen)

    def __init__(self, d, l):
        self.__data = d
        self.__local = l

        # PyQt Setup
        self.app = QApplication(sys.argv)
        self.win = QWidget()
        self.win.setWindowTitle(self.__local["window_light"])
        self.win.resize(640, 240)

        # Plot's line style
        self.pen = pg.mkPen(color=(255, 125, 0), width = 3)

        # Luz
        sansfont = QFont("Helvetica", 38)
        sansfont.setBold(True)
        title_light = QLabel()
        title_light.setText(self.__local["label_light"])
        self.value_light = QLabel()
        self.value_light.setFont(sansfont)
        self.value_light.setText("--")

        # Calibracion
        title_calibration = QLabel()
        title_calibration.setText(self.__local["label_calibration"])
        self.input_calibration = QSpinBox()
        self.input_calibration.setRange(-100, 100)
        self.input_calibration.setValue(self.__data.get_light_calibration())
        self.input_calibration.valueChanged.connect(self.valuechange)

        # Layout
        self.txt_layout = QVBoxLayout()
        self.txt_layout.addWidget(title_light)
        self.txt_layout.addWidget(self.value_light)
        self.txt_layout.addWidget(title_calibration)
        self.txt_layout.addWidget(self.input_calibration)
        self.txt_layout.addStretch()

        self.plot_layout = QVBoxLayout()
        self.plot_light = pg.plot()
        self.plot_light.setYRange(0, 1000)
        self.plot_light.hideAxis('bottom')
        self.plot_layout.addWidget(self.plot_light)

        self.app_layout = QHBoxLayout()
        self.app_layout.addLayout(self.txt_layout)
        self.app_layout.addLayout(self.plot_layout)

        # Update Timer
        timer = QtCore.QTimer()
        timer.timeout.connect(self.update)
        timer.start(self.__pooling_time)

        self.win.setLayout(self.app_layout)
        self.win.show()
        sys.exit(self.app.exec())
