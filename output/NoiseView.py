import sys

import pyqtgraph as pg
from PyQt6 import QtCore
from PyQt6.QtGui import QFont
from PyQt6.QtWidgets import QApplication, QLabel, QWidget, QHBoxLayout, QVBoxLayout


class NoiseView:
    __data = None
    __local = None
    __pooling_time = 125  # 1000 #125

    def update(self):
        self.__data.parse()

        self.value_noise.setText(str(round(self.__data.get_noise(), 1)))
        self.value_noise_rms.setText(str(round(self.__data.get_noise_rms(), 1)))

        self.plot_noise.clear()
        self.plot_noise.plot(self.__data.get_noise_series(), pen = self.pen0)

        self.plot_noise_rms.clear()
        self.plot_noise_rms.plot(self.__data.get_noise_rms_series(), pen = self.pen1)

    def __init__(self, d, l):
        self.__data = d
        self.__local = l

        # PyQt Setup
        self.app = QApplication(sys.argv)

        self.win = QWidget()
        self.win.setWindowTitle(self.__local["window_rms"])
        self.win.resize(640, 240)

        # Plot's line styles
        self.pen0 = pg.mkPen(color=(255, 125, 0), width = 3)
        self.pen1 = pg.mkPen(color=(255, 255, 0), width=3)

        # text label
        title_noise = QLabel()
        title_noise.setText(self.__local["label_rms_avg"])
        self.value_noise = QLabel()

        title_noise_rms = QLabel()
        title_noise_rms.setText(self.__local["label_rms_raw"])
        self.value_noise_rms = QLabel()

        sansfont = QFont("Helvetica", 38)
        sansfont.setBold(True)
        self.value_noise_rms.setFont(sansfont)

        self.value_noise_rms.setText("--")

        self.value_noise.setFont(sansfont)
        self.value_noise.setText("--")

        # Layout
        self.app_layout = QHBoxLayout()
        self.txt_layout = QVBoxLayout()
        self.txt_layout.addWidget(title_noise)
        self.txt_layout.addWidget(self.value_noise)
        self.txt_layout.addWidget(title_noise_rms)
        self.txt_layout.addWidget(self.value_noise_rms)
        self.txt_layout.addStretch()
        self.app_layout.addLayout(self.txt_layout)
        self.plot_layout = QVBoxLayout()
        self.plot_noise = pg.plot()
        self.plot_noise.setYRange(0, 20000)
        self.plot_noise.hideAxis('bottom')
        self.plot_layout.addWidget(self.plot_noise)
        self.plot_noise_rms = pg.plot()
        self.plot_noise_rms.setYRange(0, 20000)
        self.plot_noise_rms.hideAxis('bottom')
        self.plot_layout.addWidget(self.plot_noise_rms)

        self.app_layout.addLayout(self.plot_layout)

        # Update Timer
        timer = QtCore.QTimer()
        timer.timeout.connect(self.update)
        timer.start(self.__pooling_time)

        self.win.setLayout(self.app_layout)
        self.win.show()
        sys.exit(self.app.exec())
