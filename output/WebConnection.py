from events.event import subscribe
import requests
from requests.structures import CaseInsensitiveDict
from datetime import datetime

class WebConnection:
    __data = None
    __local = None

    def log_data(self, data):
        now: str =  str(datetime.now())
        device_id = "5e5f67759c50b60017596e2a"
        jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtYWlsIjoicGFibG8ubWFyaW5vQHVkYy5lcyIsInVpZCI6IjVlNTc5MjUyOTZkOTY0MDAxN2I4Mzc5YSIsInJvbGUiOiJBRE1JTiIsImlhdCI6MTY1MzkzMTY1OSwiZXhwIjoxNjU0NTM2NDU5fQ.dKmO7QQUV9i4yqNTk7CekfWl8G21OwV2KYg3nDKMyLc"

        url = "https://nvlog.herokuapp.com/api/devices/" + device_id + "/measurements/"

        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers['Connection'] = 'keep-alive'
        headers["Authorization"] = "Bearer " + jwt

        data = """{
                    "light": {
                        "time": " """ + now + """ ",
                        "value": " """ + ','.join(map(str,self.__data.get_light_series())) + """ "
                    },
                    "noise": {
                        "time": " """ + now + """ ",
                        "value": " """ + ','.join(map(str,self.__data.get_dba_spl_series())) + """ "
                    },
                    "temperature": {
                        "time": " """ + now + """ ",
                        "value": " """ + ','.join(map(str,self.__data.get_temperature_series())) + """ "
                    },
                    "humidity": {
                        "time": " """ + now + """ ",
                        "value": " """ + ','.join(map(str,self.__data.get_humidity_series())) + """ "
                    },
                    "co2": {
                        "time": " """ + now + """ ",
                        "value": " """ + ','.join(map(str,self.__data.get_co2_series())) + """ "
                    },
                    "voc": {
                        "time": " """ + now + """ ",
                        "value": " """ + ','.join(map(str,self.__data.get_voc_series())) + """ "
                    }
                }
        """

        print("Sending Request to",url)
        print("Request Header", headers)
        print("Request Body", data)
        resp = requests.post(url, headers=headers, data=data)

        print("Request response", resp.text)

    def update(self):
        self.__data.parse()

    def __init__(self, d, local):
        self.__data = d
        # Patron Observador para enviar datos una vez se haya completado una serie de medidas
        subscribe("sample_series_complete", self.log_data)

        #self.update()
        #self.log_data(None)

        while True:
            self.update()

