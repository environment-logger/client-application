import sys

import pyqtgraph as pg
from PyQt6 import QtCore
from PyQt6.QtGui import QFont
from PyQt6.QtWidgets import QApplication, QLabel, QWidget, QHBoxLayout, QVBoxLayout, QSpinBox


class Co2VocView:
    __data = None
    __local = None
    __pooling_time = 125  # tiempo de refresco de datos 125|1000ms

    def valuecchange(self):
        self.__data.set_co2_calibration(float(self.input_calibration_c.value()))

    def valuevchange(self):
        self.__data.set_voc_calibration(float(self.input_calibration_v.value()))

    def update(self):
        self.__data.parse()
        self.value_co2.setText(str(round(self.__data.get_co2(), 1)))
        self.value_voc.setText(str(round(self.__data.get_voc(), 1)))

        self.plot_co2.clear()
        self.plot_co2.plot(self.__data.get_co2_series(), pen =self.pen0)

        self.plot_voc.clear()
        self.plot_voc.plot(self.__data.get_voc_series(), pen = self.pen1)

    def __init__(self, d, l):
        self.__data = d
        self.__local = l

        # PyQt Setup
        self.app = QApplication(sys.argv)
        self.win = QWidget()
        self.win.setWindowTitle(self.__local["window_co2"])
        self.win.resize(640, 240)

        # Plot's line styles
        self.pen0 = pg.mkPen(color=(255, 125, 0), width = 3)
        self.pen1 = pg.mkPen(color=(255, 255, 0), width=3)
        # Co2
        sansfont = QFont("Helvetica", 38)
        sansfont.setBold(True)
        title_co2 = QLabel()
        title_co2.setText(self.__local["label_co2"])
        self.value_co2 = QLabel()
        self.value_co2.setFont(sansfont)
        self.value_co2.setText("--")
        # Calibración Co2
        title_calibration_c = QLabel()
        title_calibration_c.setText(self.__local["label_calibration"])
        self.input_calibration_c = QSpinBox()
        self.input_calibration_c.setRange(-200, 200)
        self.input_calibration_c.setValue(self.__data.get_co2_calibration())
        self.input_calibration_c.valueChanged.connect(self.valuecchange)
        # VOC
        title_voc = QLabel()
        title_voc.setText(self.__local["label_voc"])
        self.value_voc = QLabel()
        self.value_voc.setFont(sansfont)
        self.value_voc.setText("--")
        # Calibración VOC
        title_calibration_v = QLabel()
        title_calibration_v.setText(self.__local["label_calibration"])
        self.input_calibration_v = QSpinBox()
        self.input_calibration_v.setRange(-20, 20)
        self.input_calibration_v.setValue(self.__data.get_voc_calibration())
        self.input_calibration_v.valueChanged.connect(self.valuevchange)

        # Gráfica Co2
        self.plot_co2 = pg.plot()
        self.plot_co2.setYRange(0, 2000)
        self.plot_co2.hideAxis('bottom')
        # Gráfica voc
        self.plot_voc = pg.plot()
        self.plot_voc.setYRange(0, 500)
        self.plot_voc.hideAxis('bottom')

        # Layout
        self.txt_layout = QVBoxLayout()
        self.txt_layout.addWidget(title_co2)
        self.txt_layout.addWidget(self.value_co2)
        self.txt_layout.addWidget(title_calibration_c)
        self.txt_layout.addWidget(self.input_calibration_c)
        self.txt_layout.addStretch()
        self.txt_layout.addWidget(title_voc)
        self.txt_layout.addWidget(self.value_voc)
        self.txt_layout.addWidget(title_calibration_v)
        self.txt_layout.addWidget(self.input_calibration_v)
        self.txt_layout.addStretch()
        # Layout Gráficas
        self.plot_layout = QVBoxLayout()
        self.plot_layout.addWidget(self.plot_co2)
        self.plot_layout.addWidget(self.plot_voc)

        # Layout
        self.app_layout = QHBoxLayout()
        self.app_layout.addLayout(self.txt_layout)
        self.app_layout.addLayout(self.plot_layout)

        # Update Timer
        timer = QtCore.QTimer()
        timer.timeout.connect(self.update)
        timer.start(self.__pooling_time)

        self.win.setLayout(self.app_layout)
        self.win.show()
        sys.exit(self.app.exec())
