import serial

class SerialListener:
    serial_com = None
    PORT = None
    BAUDRATE = 115200

    def __init__(self, p):
        self.PORT = p;
        try:
            self.serial_com = serial.Serial(self.PORT, self.BAUDRATE)
        except:
            print("Serial listener an exception occurred")

    def line(self):
        value = None
        while value is None:
            try:
                value = str(self.serial_com.readline().decode('utf-8').rstrip())
            except:
                print("Line decoding error exception ")
        return value