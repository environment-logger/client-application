import math

import numpy as np
from typing import List
from scipy.fftpack import fftfreq as scipy_fftfreq
from scipy.fft import fft as scipy_fft
from acoustics import Signal
from acoustics.standards.iec_61672_1_2013 import weighting_function_a
from events.event import post_event


class Data:
    __serial = None

    __raw_str = ''
    __F: int = 44100
    __T: float = (1 / 44100)  # Period

    # Samples values
    __temperature: float = 0
    __humidity: float = 0
    __co2: float = 0
    __voc: float = 0
    __light: float = 0
    __noise: float = 0
    __noise_rms: float = 0
    __dba_spl : float = 0

    __temperature_calibration: float = 0
    __humidity_calibration: float = 0
    __co2_calibration: float = 0
    __voc_calibration: float = 0
    __light_calibration: float = 0
    __noise_calibration: float = 0

    # Samples stored fo the measured magnitudes, array 100 actualizando cada 125 ms son 12,5s 480 un minuto
    __SERIES_LEN: int = 100#480
    #
    __samples_stored: int = 0

    # Samples arrays
    __temperature_series: List[float] = np.zeros(__SERIES_LEN).astype(float)
    __humidity_series: List[float] = np.zeros(__SERIES_LEN).astype(float)
    __co2_series: List[float] = np.zeros(__SERIES_LEN).astype(float)
    __voc_series: List[float] = np.zeros(__SERIES_LEN).astype(float)
    __light_series: List[float] = np.zeros(__SERIES_LEN).astype(float)
    __noise_series: List[float] = np.zeros(__SERIES_LEN).astype(float)
    __noise_rms_series: List[float] = np.zeros(__SERIES_LEN).astype(float)
    __dba_spl_series: List[float] = np.zeros(__SERIES_LEN).astype(float)

    # Sound
    __SOUND_SAMPLES_LEN = 1024#3'333072
    __A_table: List[float] = np.array([])
    __sound: List[int] = np.zeros(__SOUND_SAMPLES_LEN).astype('int32')
    __frequency: List[int] = np.zeros(__SOUND_SAMPLES_LEN // 2)

    #
    __sampling_time: int = 0

    def __init__(self, serial, callibration) -> None:
        print(serial)
        self.__serial = serial
        self.__temperature_calibration = float(callibration['temperature'])
        self.__humidity_calibration = float(callibration['humidity'])
        self.__co2_calibration = float(callibration['co2'])
        self.__voc_calibration = float(callibration['voc'])
        self.__light_calibration = float(callibration['light'])
        self.__noise_calibration = float(callibration['noise'])
        self.__noise_samples = int(callibration["rmssamples"])

    # value series measurement error
    @staticmethod
    def __update_value(v, s, m, e) -> (float, List[float]):
        try:
            v = float(m)
        except ValueError:
            v = e
        if math.isnan(v):
            v = e
        s = np.roll(s, -1)
        s[-1] = v
        return v, s

    def parse(self) -> None:
        raw_values = self.__serial.line().split(",")

        if len(raw_values) == self.__SOUND_SAMPLES_LEN+6:  # Espero 3078 valores: Temp, Hum, co2, voc, light, 3072 sound samples, sampling_time

            # Update temperature
            try:
                self.__temperature, self.__temperature_series = self.__update_value(self.__temperature,
                                                                                    self.__temperature_series,
                                                                                    float(raw_values[0])+self.__temperature_calibration,
                                                                                    -1)
            except:
                print("Error parsing temperature")

            # Update humidity
            try:
                self.__humidity, self.__humidity_series = self.__update_value(self.__humidity,
                                                                              self.__humidity_series,
                                                                              float(raw_values[1])+self.__humidity_calibration,
                                                                              -1)
            except:
                print("Error parsing humidity")

            # Update co2
            try:
                self.__co2, self.__co2_series = self.__update_value(self.__co2,
                                                                    self.__co2_series,
                                                                    float(raw_values[2])+self.__co2_calibration,
                                                                    -1)
            except:
                print("Error parsing co2")

            # Update voc
            try:
                self.__voc, self.__voc_series = self.__update_value(self.__voc,
                                                                    self.__voc_series,
                                                                    float(raw_values[3])+self.__voc_calibration,
                                                                    -1)
            except:
                print("Error parsing voc")

            # Update light
            try:
                self.__light, self.__light_series = self.__update_value(self.__light,
                                                                        self.__light_series,
                                                                # valores con recta de regresion aplicada
                                                                (1.28*float(raw_values[4]))+16+self.__light_calibration,
                                                                #float(raw_values[4])+self.__light_calibration,
                                                                -1)
            except:
                print("Error parsing light")

            # Update noise
            try:
                self.__sound = np.array(raw_values[5:-2]).astype('int32')
            except:
                print("Error parsing noise")

            # Replace invalid values
            self.__sound[self.__sound > -2] = self.__sound.mean()

            # Center values around 0
            mean = self.__sound.mean()
            self.__sound = (self.__sound - mean)

            #__w = 5
            #if (self.__sound[self.__sound > -__w] < __w):
            #    self.__sound[self.__sound > -__w] = 0
            #self.__sound[self.__sound < -__w] = self.__sound[self.__sound < -__w]+__w
            #self.__sound[self.__sound > __w] = self.__sound[self.__sound > __w]-__w
            #for x in self.__sound:
            #    print(x)

            # FFT
            y = np.abs(scipy_fft(self.__sound))[:self.__SOUND_SAMPLES_LEN // 2]
            # Ponderacion A
            s = Signal(y, self.get_F())
            s = s.weigh('A', True)
            # Almaceno el rms del ruido
            __rms = s.rms()

            if(__rms>150):
                __rms = __rms-100
            else:
                __rms = __rms/1.75
            '''
            '''
            self.__noise_rms, self.__noise_rms_series = self.__update_value(self.__noise_rms, self.__noise_rms_series,
                                                                           __rms, -1)
            # Promedio los ultimos N valores
            noise_mean = np.mean(self.__noise_rms_series[-(self.__noise_samples):])
            self.__noise, self.__noise_series = self.__update_value(self.__noise, self.__noise_series,
                                                                    noise_mean, -1)

            # Calculo dBA SPL
            try:
                dba_spl = 20 * math.log10(
                    self.__noise) + self.__noise_calibration  # Utilizo el valor de sonido promediado para calcular dBA
            except:
                dba_spl = 0
            self.__dba_spl, self.__dba_spl_series = self.__update_value(self.__dba_spl,
                                                                        self.__dba_spl_series,
                                                                        dba_spl,
                                                                        -1)

            # Update Sampling time in microseconds
            try:
                self.__sampling_time = int(raw_values[-1])
            except:
                print("Error parsing sampling time")

            # lanzo evento de serie completa para los suscriptores
            self.__samples_stored = self.__samples_stored + 1
            if self.__samples_stored >= self.__SERIES_LEN:
                self.__samples_stored = 0
                # Lanzo evento
                post_event("sample_series_complete", None)

    def get_F(self) -> float:
        # Frecuencia de muestreo en herzios
        try:
            F = round(self.__SOUND_SAMPLES_LEN / self.__sampling_time * 1000000)
        except:
            F = self.__F
        return F

    # Temperature getter functions
    def get_temperature(self) -> float:
        return self.__temperature

    def get_temperature_series(self) -> List[float]:
        return self.__temperature_series

    def get_temperature_series_length(self) -> int:
        return self.__SERIES_LEN

    # Humidity getter functions
    def get_humidity(self) -> float:
        return self.__humidity

    def get_humidity_series(self) -> List[float]:
        return self.__humidity_series

    def get_humidity_series_length(self) -> int:
        return self.__SERIES_LEN

    # Co2 getter functions
    def get_co2(self) -> float:
        return self.__co2

    def get_co2_series(self) -> List[float]:
        return self.__co2_series

    def get_co2_series_length(self) -> int:
        return self.__SERIES_LEN

    # VOC getter functions
    def get_voc(self) -> float:
        return self.__voc

    def get_voc_series(self) -> List[float]:
        return self.__voc_series

    def get_voc_series_length(self) -> int:
        return self.__SERIES_LEN

    # Light getter functions
    def get_light(self) -> float:
        return self.__light

    def get_light_series(self) -> List[float]:
        return self.__light_series

    def get_light_series_length(self) -> int:
        return self.__SERIES_LEN

    # Sound
    def get_raw(self) -> str:
        return self.__raw_str

    def get_sound(self) -> List[int]:
        return self.__sound

    def get_spectrum(self) -> (List[int], List[int]):
        y = np.abs(scipy_fft(self.__sound))[:self.__SOUND_SAMPLES_LEN // 2]
        x = scipy_fftfreq(self.__SOUND_SAMPLES_LEN, self.__T)[:self.__SOUND_SAMPLES_LEN // 2]
        # Weigh
        if len(self.__A_table) == 0 and np.all(x != 0.):
            self.__A_table = weighting_function_a(x)
            y = y * self.__A_table

        return x, y

    # dBA Noise getter functions
    def get_dba_spl(self) -> float:
        '''
        try:
            dba_spl = 20 * math.log10(self.__noise)-15 # Utilizo el valor de sonido promediado para calcular dBA
        except:
            dba_spl = 0
        '''
        return self.__dba_spl

    def get_dba_spl_series(self) -> List[float]:
        return self.__dba_spl_series

    def get_dba_spl_series_lenght(self) -> float:
        return self.__SERIES_LEN

    # noise RMS getter functions
    def get_noise_rms(self) -> float:
        return self.__noise_rms

    def get_noise_rms_series(self) -> List[float]:
        return self.__noise_rms_series

    def get_noise_series_rms_length(self) -> int:
        return self.__SERIES_LEN

    # noise getter functions
    def get_noise(self) -> float:
        return self.__noise

    def get_noise_series(self) -> List[float]:
        return self.__noise_series

    def get_noise_series_length(self) -> int:
        return self.__SERIES_LEN

    # Calibration
    def get_temperature_calibration(self) -> float:
        return self.__temperature_calibration

    def set_temperature_calibration(self, v: float) -> None:
        self.__temperature_calibration = v

    def get_humidity_calibration(self) -> float:
        return self.__humidity_calibration

    def set_humidity_calibration(self, v: float) -> None:
        self.__humidity_calibration = v

    def get_co2_calibration(self) -> float:
        return self.__co2_calibration

    def set_co2_calibration(self, v: float) -> None:
        self.__co2_calibration = v

    def get_voc_calibration(self) -> float:
        return self.__voc_calibration

    def set_voc_calibration(self, v: float) -> None:
        self.__voc_calibration = v

    def get_light_calibration(self) -> float:
        return self.__light_calibration

    def set_light_calibration(self, v: float) -> None:
        self.__light_calibration = v

    def get_noise_calibration(self) -> float:
        return self.__noise_calibration

    def set_noise_calibration(self, v: float) -> None:
        self.__noise_calibration = v
