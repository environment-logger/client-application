from sensors.SerialListener import SerialListener
from sensors.Data import Data

from output.DefaultView import DefaultView
from output.NoiseDBAView import NoiseDBAView
from output.NoiseView import NoiseView
from output.TemperatureHumidityView import TemperatureHumidityView
from output.Co2VocView import Co2VocView
from output.LightView import LightView
from output.WebConnection import WebConnection

import json

if __name__ == '__main__':
    # Cargo el fichero de configuración
    with open("config.json", "r") as jsonfile:
        cfg = json.load(jsonfile)
        jsonfile.close()

    # Instancio clase de conexion al serial pasandole el puerto
    my_serial = SerialListener(cfg['port'])

    if my_serial.serial_com is not None:
        # Instancio la clase que gestiona los datos recibidos, pasándole la entrada y la configuracion de la calibración
        my_data = Data(my_serial, cfg['calibration'])

        # Instancio la vista definida en el archivo de configuración
        my_view = eval(cfg["active_view"])(my_data, cfg['localization'][cfg["language"]])