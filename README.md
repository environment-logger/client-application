# Environment logger Client 0.1.0

Client for viewing and storing environment logger's data.

![Client default view](img/DefaultView.png)

This client works on Python 3.X , to get started you need to install the following packages:
```
# pySerial
pip install pyserial

# sciPy
pip install numpy scipy matplotlib ipython jupyter pandas sympy nose

# pyQt6
pip install PyQt6

# pyQtGraph
pip install pyqtgraph

# Acoustics
pip install acoustics

# Requests
pip install requests

# Events
pip install Events

```

After that you need to configure the port of the sensor device in the *config.json* file, to get available ports run the following command:
```
# On Pc
python -m serial.tools.list_ports'

# on Mac or Linux
python3 -m serial.tools.list_ports
```

You May change the application view modifying the active_view value in the config file, available options are:
```
# View sensor's data
DefaultView
# Send sensor's data to a web service
WebConnection
# Calibrate sensors:
NoiseView, NoiseDBAView, TemperatureHumidityView, Co2VocView, LightView
```
Config file holds the parameters to calibrate the sensor if needed.
